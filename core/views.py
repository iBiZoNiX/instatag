# coding: utf-8
from django.shortcuts import render
from instagram.client import InstagramAPI
from instagram.bind import InstagramClientError, InstagramAPIError

from project.localsettings import INSTAGRAM_CLIENT_ID, INSTAGRAM_CLIENT_SECRET, INSTAGRAM_HASHTAG, EXTENSIONS


def main(request):
    """
    Returns the main page.
    """
    try:
        api = InstagramAPI(client_id=INSTAGRAM_CLIENT_ID, client_secret=INSTAGRAM_CLIENT_SECRET)
        media_list, url = api.tag_recent_media(tag_name=INSTAGRAM_HASHTAG, count=29)
    except (InstagramClientError, InstagramAPIError):
        images = []
    else:
        images = [img.get_standard_resolution_url() for img in media_list if img.get_standard_resolution_url().endswith(EXTENSIONS)]
        print len(images)

    return render(request, 'core/main.html', {'images': images})
