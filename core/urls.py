from django.conf.urls import url

from core.views import main


urlpatterns = [
    url(r'^$', main, name='main'),
]